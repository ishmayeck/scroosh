You'll probably have to edit the workflow file with at least the location of the shell script on your system. It's
[here](https://gitlab.com/ishmayeck/scroosh/-/blob/master/Upload%20Screenshot.workflow/Contents/document.wflow#L62).

Double click the workflow file to install it. Then it'll show up as a service 
and you'll be able to create a global keybind for it.


![](settings.png)