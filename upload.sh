#!/usr/bin/env bash

file=ss-$(date +%Y%m%d%H%M%S).png
screencapture -i /tmp/${file}
if [[ -f /tmp/${file} ]]; then
    scp /tmp/${file} vega.ishmayeck.net:/srv/www/vodka/i/
    echo https://image.likes.vodka/i/${file} | pbcopy
    afplay /System/Library/Sounds/Purr.aiff
    rm /tmp/${file}
fi